﻿using Microsoft.Data.Entity;

namespace D08.Models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Location> Locations { get; set; }
        public DbSet<Movie> Movies { get; set; }
    }
}
