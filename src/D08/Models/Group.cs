﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace D08.Models
{
    public class Group {
        public Group() {
            this.Roles = new HashSet<ApplicationRoleGroup>();
        }


        public Group(string name) : this() {
            this.Name = name;
        }


        [Key]
        [Required]
        public int Id { get; set; }

        public string Name { get; set; }
        public virtual HashSet<ApplicationRoleGroup> Roles { get; set; }
    }
}
