﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;

namespace D08.Models {
    public static class AppSeed    {
        public static void Initialize(IServiceProvider serviceProvider, string appPath) {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if(context == null) { throw new Exception("DB is null"); }

            // get rid of old content, if any
            context.Movies.RemoveRange(context.Movies);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedMoviesFromCsv(relPath, context);
         }

        private static void SeedMoviesFromCsv(string relPath, ApplicationDbContext context) {
            string source = relPath + "movie.csv";
            if (!File.Exists(source)) {
                throw new Exception("Cannot find file " + source);
            }
            Movie.ReadAllFromCSV(source);
            List<Movie> lst = Movie.ReadAllFromCSV(source);
            context.Movies.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, ApplicationDbContext context) {
            string source = relPath + "location.csv";
            if (!File.Exists(source)) {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }
    }
}
