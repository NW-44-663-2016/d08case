﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace D08.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser { 

   public ApplicationUser() : base()
        {
        this.Groups = new HashSet<ApplicationUserGroup>();
    }


    [Required]
    public string FirstName { get; set; }

    [Required]
    public string LastName { get; set; }


    public virtual ICollection<ApplicationUserGroup> Groups { get; set; }
}

}
