﻿using Xunit;
using Microsoft.AspNet.Mvc;
using D08.Controllers;

namespace D08.Test {

    public class HomeControllerTest
    {

        [Fact]
        public void About_CreatesView() {  // return type is void not async task
            // Arrange
            var controller = new HomeController();  // home controller doesn't use dbcontext

            // Act
            var result = controller.About(); // controller uses sync methods (not async)

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.Null(viewResult.ViewName);
            Assert.NotNull(viewResult.ViewData);
        }

        [Fact]
        public void Contact_CreatesView() {
            // Arrange
            var controller = new HomeController();

            // Act
            var result = controller.Contact();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            Assert.NotNull(viewResult);
            Assert.NotNull(viewResult.ViewData);
            Assert.Equal("Your contact page.", viewResult.ViewData["Message"]);
        }
    }
}
