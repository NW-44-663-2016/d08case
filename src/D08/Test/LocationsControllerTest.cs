﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.DependencyInjection;
using D08.Models;
using Xunit;
using Microsoft.AspNet.Mvc;
using D08.Controllers;
using Microsoft.Data.Entity;

namespace D08.Test
{
    public class LocationsControllerTest
    {
        // use dependency injection - create a local IServiceProvider
        private readonly IServiceProvider _serviceProvider;

        // create a constructor to set things up
        public LocationsControllerTest() {
            var efServiceProvider = new ServiceCollection();

            var services = new ServiceCollection();

            services.AddEntityFramework()
            .AddInMemoryDatabase()
            .AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase());

            _serviceProvider = services.BuildServiceProvider();
        }

        [Fact]
        public void GetLocations() {
            // Arrange
            int numDummyObjects = 3;
            var dbContext = _serviceProvider.GetRequiredService<ApplicationDbContext>();
            CreateTestLocations(numberOfLocations: numDummyObjects, dbContext: dbContext);
            var controller = new LocationsController(dbContext);
            
            // Act
            var result = controller.GetLocations();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(numDummyObjects, result.Count<Location>());
        }
  

        private static DbSet<Location> CreateTestLocations(int numberOfLocations, ApplicationDbContext dbContext) {

            var locations = Enumerable.Range(1, numberOfLocations).Select(n =>
                 new Location() {
                     LocationID = n,
                     Country = "Country " + n,
                     Place = "Place " + n,
                     Longitude = n,
                     Latitude = n,
                 });

            dbContext.AddRange(locations);
            dbContext.SaveChanges();
            return dbContext.Locations;
        }
    }
}
