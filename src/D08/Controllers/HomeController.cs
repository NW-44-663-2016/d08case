﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Xunit;

namespace D08.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        int Add(int x, int y) {

            return x + y;

        }

        [Fact]
        public void EqualTest() {
            Assert.Equal(4, Add(2, 2));
        }

        [Fact]
        public void NotEqualTest() {
            Assert.NotEqual(5, Add(2, 2));
        }
        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(1, 1, 2)]
        [InlineData(1, 2, 3)]
        [InlineData(-1, -1, -2)]
        [InlineData(-1, 1, 0)]
        public void TheoryAddExample(int a, int b, int c) {
            Assert.Equal(c, Add(a, b));
        }




    }
}
