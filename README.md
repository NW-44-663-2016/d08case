# 44-663: Instructions

ASP.NET 5: Web API

Practice creating RESTful APIs 

[Northwest Online](http://www.nwmissouri.edu/online/)




## We will use:

*   ASP.NET 5
*   MVC 6
*   Entity Framework 7
*   SQL Server LocalDB

## D08: Create an initial app, D08Yourname

*   Create new ASP.NET Web App with ASP.NET 5 App template.
*   Create your custom data class (see A06).
*   Create a simple DbContext with a DbSet for your class.
*   Seed some starting data (you can use your earlier assignment).
*   You can add the location class if you like (so the seed data will work).
*   Update Startup.cs to remove Identity and call your seed method. 
*   Add an initial migration & update database
*   Run your app.
*   If you turned off the automatic page opening, you'll have to open your browser manually.
*   Open browser to localhost:8888 where 8888 is your port number. 
*   Refresh and view your tables in the Visual Studio SQL Server Object Explorer (SSOE).
*   Make sure it runs and post a picture of your new application D08Yourname running along with a picture of your tables and/or data in the SQL Server Object Explorer window.


## C08: Add an API controller to D08Yourname

*  See the instructions in the slides to add a new API Controller for your class.
*  Run your app. Open a browser and point it to localhost:8888/api/movies, where 8888 is your portnumber and api/movies is the name of your resource (likely not movies). 
*  If you have two models, you might want to create one API controller normally - and one using async methods - and compare them.
*  Post a screen shot of your D08Yourname running with a browser showing the JSON data returned by the HTTP verb GET. 



## A07 - Install Postman and explore your API

*  See the instructions in the slides to install Postman.
*  Start your web app and launch Postman.
*  Use Postman to explore GET localhost:8888/api/movies, where 8888 is your portnumber and api/movies is the name of your resource (likely not movies). 
*  Post a screen shot of your Postman running with a browser showing the JSON data returned by the HTTP verb GET. 
*  Commit your code in our BitBucket team repo under D08 (or create private one and invite us if you prefer). Post a link to your repo along with the screen shot.

## To run this version

To run this version, you'll want to set up your own database.  If needed, set the version:

```
dnvm use 1.0.0-rc1-final
dnu restore
```

First, delete the Migrations folder and the database (using SQL Server Object Explorer). Then, open a command window in the project folder, and create a new intial migration and update your database.

```
dnx ef migrations add initial
dnx ef database update
```

Refresh your database in the SQL Server Object Explorer. You should see the Location table and the Movie table.  

[Northwest Online](http://www.nwmissouri.edu/online/)

## Examples

![](images/D08Yourname.png) 

![](images/D08_API_GET_response_locations.png) 

![](images/D08_API_GET_response_movies.png) 

![](images/postman.png)